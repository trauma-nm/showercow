# showercow

Yoinking showerthoughts from reddit and yeeting them into cowsay

## Dependencies

- sh
- curl
- jq
- shuf
- sed
- cowsay

## Installation

```
git clone https://gitlab.com/trauma-nm/showercow.git
cd ./showercow
chmod +x showercow
```

## Tip

Add showercow to your bashrc to get a refreshing thought everytime you open a terminal!
